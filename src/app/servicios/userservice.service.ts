import { Injectable } from '@angular/core';
import { UsuarioDataI } from '../components/interface/datausuarios';

@Injectable({
  providedIn: 'root'
})
export class UserserviceService {

  listUsuarios: UsuarioDataI[] = [
    {usuario:'jaimito', numTar: '1234567891234567', fecha: '16/22' , cvv: '16/22'},
    {usuario:'jaimito', numTar: '1234567891004567', fecha: '16/29', cvv: '16/22'},
    {usuario:'jaimito', numTar: '1234567891234567', fecha: '16/22', cvv: '16/22'},
    {usuario:'jaimito', numTar: '1234567891234567', fecha: '16/22', cvv: '16/22'},
    {usuario:'jaimmito', numTar: '1234567891234567', fecha: '16/22', cvv: '16/22'},
  ];
  constructor() { }

  agregarUsuario(user:UsuarioDataI):void{
    this.listUsuarios.unshift(user);

  }

  getUsuario():UsuarioDataI[] {
    //slice retorna una copia del array
    return this.listUsuarios.slice();
  }
  eliminarUsuario(usuario:string){
  
    this.listUsuarios = this.listUsuarios.filter(data => {
      return data.usuario !== usuario;
    });
  }

  modificarUsuario(user: UsuarioDataI){
    this.eliminarUsuario(user.usuario);
    this.agregarUsuario(user);
  }
}
