import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LitadoDeTarjetaComponent } from './litado-de-tarjeta.component';

describe('LitadoDeTarjetaComponent', () => {
  let component: LitadoDeTarjetaComponent;
  let fixture: ComponentFixture<LitadoDeTarjetaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LitadoDeTarjetaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LitadoDeTarjetaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
