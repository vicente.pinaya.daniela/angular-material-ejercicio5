import { Component, OnInit } from '@angular/core';
import { UserserviceService } from 'src/app/servicios/userservice.service';
import { UsuarioDataI } from '../interface/datausuarios';

@Component({
  selector: 'app-litado-de-tarjeta',
  templateUrl: './litado-de-tarjeta.component.html',
  styleUrls: ['./litado-de-tarjeta.component.css']
})
export class LitadoDeTarjetaComponent implements OnInit {
  listUsuarios: UsuarioDataI[] = [];

  constructor(private _usuarioService: UserserviceService) { 

    this.cargarUsuarios()
    console.log( this.listUsuarios);
    

  }

  ngOnInit(): void {

  }

  cargarUsuarios(){
    this.listUsuarios = this._usuarioService.getUsuario();
  
  }

  eliminarUsuario(usuario:string){
    const opcion = confirm('Estas seguro de eliminar el usuario?');
    if(opcion){   
       this._usuarioService.eliminarUsuario(usuario);
      this.cargarUsuarios();
  
      // this._snackbar.open('El usuario fue eliminado con exito', '',{
      //   duration: 1500,
      //   horizontalPosition: 'center',
      //   verticalPosition: 'bottom',
      // });
      
    }
   
  } 

  modificarUsuario(usuario: string){
    console.log(usuario);
    //this.router.navigate(['dashboard/crear-usuario', usuario]);
  }

}


