import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {


  form!: FormGroup;
  name: string [] = []
  datos!: string;

  constructor(private fb: FormBuilder) {
    this.crearFormulario();
  }

  get informacion() {
    return this.form.get('informacion') as FormArray;
  }

  ngOnInit(): void {
  }


  crearFormulario(): void {
    this.form = this.fb.group({
      nombre: [''],
      informacion: this.fb.array([])
    })
  }


  adicionar():void {
    console.log('adicionando');
    console.log(this.form.value.nombre);

    this.name.push(this.form.value.nombre)

    this.informacion.push(this.fb.control(''))

    this.form.value.nombre = ''
  }

  borrarcheckbox(i:number): void {
    this.informacion.removeAt(i);
  }

  vaciarcaja():void {
    console.log(this.datos);

    this.datos = ''
  }

  guardar():void {
    console.log('guardado');
    this.datos = 'Id:' + this.name + ' - Nombre:' + this.name
    
  }

}
 